Rails.application.routes.draw do
  get 'page/index'
  devise_for :users
  resources :teste_devices
  
  root to: "page#index"


  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
