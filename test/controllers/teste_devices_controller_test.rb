require 'test_helper'

class TesteDevicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @teste_device = teste_devices(:one)
  end

  test "should get index" do
    get teste_devices_url
    assert_response :success
  end

  test "should get new" do
    get new_teste_device_url
    assert_response :success
  end

  test "should create teste_device" do
    assert_difference('TesteDevice.count') do
      post teste_devices_url, params: { teste_device: { teste: @teste_device.teste } }
    end

    assert_redirected_to teste_device_url(TesteDevice.last)
  end

  test "should show teste_device" do
    get teste_device_url(@teste_device)
    assert_response :success
  end

  test "should get edit" do
    get edit_teste_device_url(@teste_device)
    assert_response :success
  end

  test "should update teste_device" do
    patch teste_device_url(@teste_device), params: { teste_device: { teste: @teste_device.teste } }
    assert_redirected_to teste_device_url(@teste_device)
  end

  test "should destroy teste_device" do
    assert_difference('TesteDevice.count', -1) do
      delete teste_device_url(@teste_device)
    end

    assert_redirected_to teste_devices_url
  end
end
