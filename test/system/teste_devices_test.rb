require "application_system_test_case"

class TesteDevicesTest < ApplicationSystemTestCase
  setup do
    @teste_device = teste_devices(:one)
  end

  test "visiting the index" do
    visit teste_devices_url
    assert_selector "h1", text: "Teste Devices"
  end

  test "creating a Teste device" do
    visit teste_devices_url
    click_on "New Teste Device"

    fill_in "Teste", with: @teste_device.teste
    click_on "Create Teste device"

    assert_text "Teste device was successfully created"
    click_on "Back"
  end

  test "updating a Teste device" do
    visit teste_devices_url
    click_on "Edit", match: :first

    fill_in "Teste", with: @teste_device.teste
    click_on "Update Teste device"

    assert_text "Teste device was successfully updated"
    click_on "Back"
  end

  test "destroying a Teste device" do
    visit teste_devices_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Teste device was successfully destroyed"
  end
end
