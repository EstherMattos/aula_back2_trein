class TesteDevicesController < ApplicationController
  before_action :set_teste_device, only: [:show, :edit, :update, :destroy]

  # GET /teste_devices
  # GET /teste_devices.json
  def index
    @teste_devices = TesteDevice.all
  end

  # GET /teste_devices/1
  # GET /teste_devices/1.json
  def show
  end

  # GET /teste_devices/new
  def new
    @teste_device = TesteDevice.new
  end

  # GET /teste_devices/1/edit
  def edit
  end

  # POST /teste_devices
  # POST /teste_devices.json
  def create
    @teste_device = TesteDevice.new(teste_device_params)

    respond_to do |format|
      if @teste_device.save
        format.html { redirect_to @teste_device, notice: 'Teste device was successfully created.' }
        format.json { render :show, status: :created, location: @teste_device }
      else
        format.html { render :new }
        format.json { render json: @teste_device.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teste_devices/1
  # PATCH/PUT /teste_devices/1.json
  def update
    respond_to do |format|
      if @teste_device.update(teste_device_params)
        format.html { redirect_to @teste_device, notice: 'Teste device was successfully updated.' }
        format.json { render :show, status: :ok, location: @teste_device }
      else
        format.html { render :edit }
        format.json { render json: @teste_device.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teste_devices/1
  # DELETE /teste_devices/1.json
  def destroy
    @teste_device.destroy
    respond_to do |format|
      format.html { redirect_to teste_devices_url, notice: 'Teste device was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teste_device
      @teste_device = TesteDevice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def teste_device_params
      params.require(:teste_device).permit(:teste)
    end
end
